var bunyan = require('bunyan'),
    request = require('request-json');

var log = bunyan.createLogger({name: "ushbullet",src: false}),
    client = request.createClient('https://api.pushbullet.com');

var title, type;

module.exports = {
    configure: function(_token, _title, _type) {
        client.headers['Access-Token'] = _token;
        client.headers['Content-Type'] = 'application/json';

        title = _title;
        type = _type;
    },
    process: function(message) {
        var data = {"body":message,"title": title, "type": type};

        client.post('/v2/pushes', data, function(err, res, body) {
            return log.info(body);
        });

    }
};
